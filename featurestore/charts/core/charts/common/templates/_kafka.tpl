{{/*
Kafka topic template
*/}}
{{- define "common.kafka.config" -}}
messaging.kafka.{{ .ConfigName }}.topic={{ .TopicName }}
{{- if ((.TopicConfig).numPartitions) }}
messaging.kafka.{{ .ConfigName }}.num-partitions={{ .TopicConfig.numPartitions }}
{{- end }}
{{- if ((.TopicConfig).replicationFactor) }}
messaging.kafka.{{ .ConfigName }}.replication-factor={{ .TopicConfig.replicationFactor }}
{{- end }}
{{- if ((.TopicConfig).retentionMs) }}
messaging.kafka.{{ .ConfigName }}.retention.ms={{ .TopicConfig.retentionMs }}
{{- end }}
{{- if ((.TopicConfig).retentionMinutes) }}
messaging.kafka.{{ .ConfigName }}.retention.minutes={{ .TopicConfig.retentionMinutes }}
{{- end }}
{{- if ((.TopicConfig).retentionHours) }}
messaging.kafka.{{ .ConfigName }}.retention.hours={{ .TopicConfig.retentionHours }}
{{- end }}
{{- end }}
