## Global Chart parameters
##
global:
  ## @param global.environment Environment name
  ## @param global.cloudProvider Select the cloud provider in which the chart will be deployed [aws, azure, local]
  ## @param global.onlineEnabled Deploy Feature-Store Online services
  ## @param global.ingressEnabled Enable ingress record generation for Feature-Store
  ## @param global.telemetryEnabled Enable telemetry generation for Feature-Store
  ##
  environment: local
  cloudProvider: "local"
  onlineEnabled: false
  ingressEnabled: false
  telemetryEnabled: false

  ## @param global.imagePullSecrets Docker registry secret names as an array
  ##
  imagePullSecrets: []

  ## @param global.kafka.username Kafka username
  ## @param global.kafka.password Kafka password
  ##
  kafka:
    username:
    password:

   ## @param global.online.databaseBackend Database Provider for online feature store [redis, mongo]
   ## @param global.online.redis.nodes. List of redis nodes in format "host:port". Cluster connection is created in case more than one node is provided
   ## @param global.online.redis.database Redis database
   ## @param global.online.redis.secret Redis secret used by all nodes
   ## @param global.online.redis.isCluster Force cluster connection in case only one node is provided in @param global.redis.nodes
   ## @param global.online.redis.isSecure Enable SSL/TLS connection to the cluster
   ## @param global.online.mongo.connectionString MongoDB connection string
   ## @param global.online.mongo.database MongoDB database name
   ## @param global.online.mongo.username MongoDB username
   ## @param global.online.mongo.password MongoDB password
   ##
  online:
    databaseBackend:
    redis:
      nodes: []
      database: 0
      secret:
      isCluster:
      isSecure:
    mongo:
      connectionString:
      database:
      username:
      password:

  ## @param global.cache.username Backend Storage username
  ## @param global.cache.password Backend Storage password
  ## Required for Azure cloud provider only
  ##
  cache:
    username:
    password:

  ## @param global.config.cacheBackend Storage Provider [datalakegen2, s3]
  ## @param global.config.s3.endpoint S3 Endpoint URL (required if Cache Backend is S3)
  ## @param global.config.s3.region S3 Region (required if Cache Backend is S3)
  ## @param global.config.s3.sessionRoleArn Role ARN to assume temporary access to S3 (required if Cache Backend is S3 and Minio is not used as cache proxy)
  ## @param global.config.s3.kmsEnabled Flag should be set to true if Cache Bucket in S3 is encrypted by KMS
  ## @param global.config.rootBucket Name of the bucket/container of root container. If defined, data/retrieve/artifacts/temporary will be handled as folders in that bucket, otherwise will be handled as individual buckets
  ## @param global.config.dataBucket Name of the bucket/container or folder if cache.bucket.root is defined for main cache
  ## @param global.config.artifactsBucket Name of the bucket/container or folder if cache.bucket.root is defined for artifacts cache
  ## @param global.config.retrieveBucket Name of the bucket/container or folder if cache.bucket.root is defined for retrieve cache
  ## @param global.config.tempBucket Name of the bucket/container or folder if cache.bucket.root is defined for temporary cache
  ## @param global.config.onlineStoreDataBucket Name of the data bucket/folder for online to offline storage
  ## @param global.config.notifications.channels Name of the Notification Channels where notifications will be send. Allowed values [kafka,logs]. If empty notifications are disabled
  ## @param global.config.sparkHistory.enabled Enable Spark history
  ## @param global.config.sparkHistory.eventsBucket Bucket where spark events will be written for spark history
  ## @param global.config.messaging.kafka.topicModificationEnabled If enable topics will be created/updated automatically
  ## @param global.config.messaging.kafka.tlsEnabled User secure connection to access kafka
  ## @param global.config.messaging.kafka.bootstrapServers Kafka Bootstrap Servers
  ## @param global.config.messaging.kafka.topics.notifications Kafka Notifications Topic Name
  ## @param global.config.messaging.kafka.topics.jobUpdates Kafka Job Updates Topic Name
  ## @param global.config.messaging.kafka.topics.onlineInput Kafka Online Input Topic Name
  ## @param global.config.messaging.kafka.topics.onlineOfflineIngest Kafka Online Offline Ingest Topic Name
  ## @param global.config.messaging.kafka.topics.onlineStorageCleanup Kafka Online Storage Cleanup Topic Name
  ## @param global.config.messaging.kafka.topics.modificationEvents Kafka Modification Events Topic Name
  ## @param global.config.messaging.kafka.topics.telemetry Kafka Telemetry Events Topic Name
  ## @param global.config.messaging.kafka.topics.telemetryAggregated Kafka Telemetry Aggregated Events Topic Name
  ## @param global.config.messaging.kafka.topicsConfig.notifications.numPartitions Number of partitions for notifications topic
  ## @param global.config.messaging.kafka.topicsConfig.notifications.replicationFactor Replication factor for notifications topic
  ## @param global.config.messaging.kafka.topicsConfig.notifications.retentionMs Retention policy for notifications topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.notifications.retentionMinutes Retention policy for notifications topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.notifications.retentionHours Retention policy for notifications topic. Expressed in hours
  ## @param global.config.messaging.kafka.topicsConfig.jobUpdates.numPartitions Number of partitions for jobUpdates topic
  ## @param global.config.messaging.kafka.topicsConfig.jobUpdates.replicationFactor Replication factor for jobUpdates topic
  ## @param global.config.messaging.kafka.topicsConfig.jobUpdates.retentionMs Retention policy for jobUpdates topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.jobUpdates.retentionMinutes Retention policy for jobUpdates topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.jobUpdates.retentionHours Retention policy for jobUpdates topic. Expressed in hours
  ## @param global.config.messaging.kafka.topicsConfig.onlineInput.numPartitions Number of partitions for onlineInput topic
  ## @param global.config.messaging.kafka.topicsConfig.onlineInput.replicationFactor Replication factor for onlineInput topic
  ## @param global.config.messaging.kafka.topicsConfig.onlineInput.retentionMs Retention policy for onlineInput topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.onlineInput.retentionMinutes Retention policy for onlineInput topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.onlineInput.retentionHours Retention policy for onlineInput topic. Expressed in hours
  ## @param global.config.messaging.kafka.topicsConfig.onlineOfflineIngest.numPartitions Number of partitions for onlineOfflineIngest topic
  ## @param global.config.messaging.kafka.topicsConfig.onlineOfflineIngest.replicationFactor Replication factor for onlineOfflineIngest topic
  ## @param global.config.messaging.kafka.topicsConfig.onlineOfflineIngest.retentionMs Retention policy for onlineOfflineIngest topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.onlineOfflineIngest.retentionMinutes Retention policy for onlineOfflineIngest topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.onlineOfflineIngest.retentionHours Retention policy for onlineOfflineIngest topic. Expressed in hours
  ## @param global.config.messaging.kafka.topicsConfig.onlineStorageCleanup.numPartitions Number of partitions for onlineStorageCleanup topic
  ## @param global.config.messaging.kafka.topicsConfig.onlineStorageCleanup.replicationFactor Replication factor for onlineStorageCleanup topic
  ## @param global.config.messaging.kafka.topicsConfig.onlineStorageCleanup.retentionMs Retention policy for onlineStorageCleanup topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.onlineStorageCleanup.retentionMinutes Retention policy for onlineStorageCleanup topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.onlineStorageCleanup.retentionHours Retention policy for onlineStorageCleanup topic. Expressed in hours
  ## @param global.config.messaging.kafka.topicsConfig.modificationEvents.numPartitions Number of partitions for modificationEvents topic
  ## @param global.config.messaging.kafka.topicsConfig.modificationEvents.replicationFactor Replication factor for modificationEvents topic
  ## @param global.config.messaging.kafka.topicsConfig.modificationEvents.retentionMs Retention policy for modificationEvents topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.modificationEvents.retentionMinutes Retention policy for modificationEvents topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.modificationEvents.retentionHours Retention policy for modificationEvents topic. Expressed in hours
  ## @param global.config.messaging.kafka.topicsConfig.telemetry.numPartitions Number of partitions for telemetry topic
  ## @param global.config.messaging.kafka.topicsConfig.telemetry.replicationFactor Replication factor for telemetry topic
  ## @param global.config.messaging.kafka.topicsConfig.telemetry.retentionMs Retention policy for telemetry topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.telemetry.retentionMinutes Retention policy for telemetry topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.telemetry.retentionHours Retention policy for telemetry topic. Expressed in hours
  ## @param global.config.messaging.kafka.topicsConfig.telemetryAggregated.numPartitions Number of partitions for telemetry aggregated topic
  ## @param global.config.messaging.kafka.topicsConfig.telemetryAggregated.replicationFactor Replication factor for telemetry aggregated topic
  ## @param global.config.messaging.kafka.topicsConfig.telemetryAggregated.retentionMs Retention policy for telemetry aggregated topic. Expressed in ms
  ## @param global.config.messaging.kafka.topicsConfig.telemetryAggregated.retentionMinutes Retention policy for telemetry aggregated topic. Expressed in minutes
  ## @param global.config.messaging.kafka.topicsConfig.telemetryAggregated.retentionHours Retention policy for telemetry aggregated topic. Expressed in hours
  ##
  config:
    cacheBackend:
    s3:
      endpoint:
      region:
      sessionRoleArn:
      kmsEnabled: false
    rootBucket:
    dataBucket:
    artifactsBucket:
    retrieveBucket:
    tempBucket:
    onlineStoreDataBucket:
    sparkHistory:
      enabled: false
      eventsBucket:
    notificationsChannels: []
    messaging:
      kafka:
        topicModificationEnabled: false
        tlsEnabled: true
        bootstrapServers:
        topics:
          notifications:
          jobUpdates:
          onlineInput:
          onlineOfflineIngest:
          onlineStorageCleanup:
          modificationEvents:
          telemetry:
          telemetryAggregated:
        topicsConfig:
          notifications:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:
          jobUpdates:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:
          onlineInput:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:
          onlineOfflineIngest:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:
          onlineStorageCleanup:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:
          modificationEvents:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:
          telemetry:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:
          telemetryAggregated:
            numPartitions:
            replicationFactor:
            retentionMs:
            retentionMinutes:
            retentionHours:

## Core service deployment parameters
##
core:
  ## @param core.rbac.create Specifies whether RBAC resources should be created
  ##
  rbac:
    create: true

  ## @param core.replicaCount Number of Core Service replicas
  ##
  replicaCount: 2

  ## @param core.commonLabels Labels to add to all deployed objects
  ##
  commonLabels: {}

  ## Core Service Autoscaling configuration
  ## ref: https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
  ## @param autoscaling.enabled Enable Horizontal POD autoscaling for Core Service
  ## @param autoscaling.minReplicas Minimum number of Core Service replicas
  ## @param autoscaling.maxReplicas Maximum number of Core Service replicas
  ## @param autoscaling.targetCPU Target CPU utilization percentage
  ## @param autoscaling.targetMemory Target Memory utilization percentage
  ##
  autoscaling:
    enabled: false
    minReplicas: 2
    maxReplicas: 10
    targetCPU: 50
    targetMemory: 50

  ## @param core.database.username MongoDB username
  ## @param core.database.password MongoDB password
  ##
  database:
    username:
    password:

  ## @param core.core.salt Random value which will be used to encode PATs
  ## @param core.core.jobsCredentialsKey Random value which will be used to encode user credentials
  ## @param core.core.idpClientSecret IdP Client Secret
  ##
  core:
    salt:
    jobsCredentialsKey:
    idpClientSecret:

  ## @param core.onlinestore.enabled Enable support for storing feature-sets in redis (online store)
  ## @param core.onlinestore.username Online storage username
  ## @param core.onlinestore.password Online storage password
  ## @param core.onlinestore.baseUrl Base url for online features
  ##
  onlinestore:
    enabled: false
    username:
    password:
    baseUrl:

  ## @param core.docker.image Core Service Docker image path
  ## @param core.docker.tag Core Service image tag
  ##
  docker:
    image: "h2oai/feature-store-core"
    tag:

  ## @param core.config.opaHost Host to open policy agent
  ## @param core.config.opaPort Port to open policy agent
  ## @param core.config.idpMetadataUri URI to openid-configuration file
  ## @param core.config.idpClientId IdP Client ID
  ## @param core.config.idpPublicMetadataUri URI to openid-configuration file for public
  ## @param core.config.idpPublicClientId IdP Client ID for public
  ## @param core.config.idpRedirectUri IdP redirect uri
  ## @param core.config.idpScopes IdP scopes list
  ## @param core.config.idpIdClaimKey JWT Claim name which will be used as user id
  ## @param core.config.idpRolesClaimKey Name of claim storing roles
  ## @param core.config.idpAdminRoleName Name of role to identify user as admin
  ## @param core.config.dbConnectionString MongoDB connection string
  ## @param core.config.databaseName MongoDB database name
  ## @param core.config.jobsCollectionTTLDays TTL for finished jobs
  ## @param core.config.disableStatistics Disable statistics computation
  ## @param core.config.validationNamingRegex Regular expression used for validating names for projects and featuresets
  ## @param core.config.userNameAttribute Spark pod username attribute
  ## @param core.config.spark.failureRetries Number of retries in case of job failure
  ## @param core.config.spark.submissionFailureRetries Number of retries in case submission failure
  ## @param core.config.spark.restartPolicy Restart policy mode [onfailure, never]
  ## @param core.config.spark.submissionFailureIntervalSeconds Linear backoff interval (in seconds) between submission retries
  ## @param core.config.spark.failureIntervalSeconds Linear backoff interval (in seconds) between retries in case of job failure
  ## @param core.config.spark.ttlSeconds Delete finished jobs (k8's resources) older than TTL value (in seconds)
  ## @param core.config.memoryCache.featureSets.evictionTimeoutMinutes Timeout in minutes after feature sets stored in the memory cache are evicted
  ## @param core.config.autoProjectOwners List e-mail addresses which are added as editors for each project created. The users provided in this list must first login for this functionality to happen
  ## @param core.config.autoProjectEditors List e-mail addresses which are added as editors for each project created. The users provided in this list must first login for this functionality to happen
  ## @param core.config.defaultProjectsPageSize Default page size for listing projects
  ## @param core.config.defaultFeatureSetsPageSize Default page size for listing feature sets
  ##
  config:
    opaHost: "localhost"
    opaPort: 8181
    idpMetadataUri:
    idpClientId:
    idpPublicMetadataUri:
    idpPublicClientId:
    idpRedirectUri:
    idpScopes:
    idpIdClaimKey: oid
    idpRolesClaimKey: roles
    idpAdminRoleName: admin
    dbConnectionString:
    databaseName:
    jobsCollectionTTLDays:
    disableStatistics: false
    validationNamingRegex: ".*"
    spark:
      userNameAttribute: id
      failureRetries: 2
      submissionFailureRetries: 3
      restartPolicy: onfailure
      submissionFailureIntervalSeconds:
      failureIntervalSeconds:
      ttlSeconds:
    memoryCache:
      featureSets:
        evictionTimeoutMinutes: 10
    autoProjectOwners: []
    autoProjectEditors: []
    defaultProjectsPageSize:
    defaultFeatureSetsPageSize:

  ## @param core.extraContainerVolumes Volumes that can be used in deployment pods
  ##
  extraContainerVolumes: []
  # - name: log4j-config
  #   mountPath: /opt/h2oai/config/
  #   subPath: log4j2.properties # (optional)
  #   configMap: log4j-config
  #   readOnly: true

  ## @param core.env Extra environment variables that will be pass onto deployment pods
  ##
  env: {}

  ## @param core.service.api.annotations Additional annotations for the API Service resource.
  ## @param core.service.web.annotations Additional annotations for the Web Service resource.
  ##
  service:
    api:
      annotations:
    web:
      annotations:

  ## @param core.nodeSelector Node labels for update Core pods assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  ##
  nodeSelector: {}

  ## @param core.tolerations Tolerations for update Core pods assignment
  ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  ##
  tolerations: []

  ## Core containers' resource requests and limits
  ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
  ## @param core.resources.limits The resources limits for the Controller container
  ## @param core.resources.requests The requested resources for the Controller container
  ##
  resources:
    limits: {}
    requests: {}

## Spark Operator service deployment parameters
##
sparkoperator:
  ## @param sparkoperator.rbac.create Specifies whether RBAC resources should be created
  ##
  rbac:
    create: true

  ## @param sparkoperator.updateCrds Automatically update CRD objects for Spark Jobs
  ##
  updateCrds: true

  ## @param sparkoperator.commonLabels Labels to add to all deployed objects
  ##
  commonLabels: {}

  ## @param sparkoperator.docker.image Spark Operator Service Docker image path
  ## @param sparkoperator.docker.tag Spark Operator Service image tag
  ##
  docker:
    image: "h2oai/feature-store-spark-operator"
    tag:

  ## @param sparkoperator.driverlessAiLicenseKey Driverless ai license key used for mojo transformation in derived feature sets
  driverlessAiLicenseKey:

  ## @param sparkoperator.config.spark.docker.image Spark Job Docker image path
  ## @param sparkoperator.config.spark.docker.tag Spark Job Docker image label/tag
  ## @param sparkoperator.config.spark.podCreateTimeout Pod creation timeout in minutes
  ## @param sparkoperator.config.spark.pullImagePolicy Pull docker image policy [Always, IfNotPresent, Never]
  ## @param sparkoperator.config.spark.logLevel Log Level for the Spark Jobs. [ERROR, WARN, INFO, DEBUG, TRACE, FATAL]
  ## @param sparkoperator.config.spark.minExecutors Minimum number of Spark executors to be used
  ## @param sparkoperator.config.spark.maxExecutors Maximum number of Spark executors to be used
  ## @param sparkoperator.config.spark.extraOptions Spark properties, ref: https://spark.apache.org/docs/latest/running-on-kubernetes.html#spark-properties
  ##
  config:
    spark:
      docker:
        image: "h2oai/feature-store-spark"
        Tag:
      podCreateTimeoutMinutes: 5
      pullImagePolicy:
      logLevel: INFO
      minExecutors: 1
      maxExecutors: 5
      ## example:
      ## extraOptions: |
      ##   spark.kubernetes.driver.request.cores=2 \
      ##   spark.kubernetes.driver.limit.cores=2 \
      ##   spark.kubernetes.executor.request.cores=2 \
      ##   spark.kubernetes.executor.limit.cores=2
      extraOptions:

  ## @param sparkoperator.extraConfigmapMounts configmaps to mount in core pods
  ##
  extraConfigmapMounts: []
  # - name: log4j-config
  #   mountPath: /opt/h2oai/config/
  #   subPath: log4j2.properties # (optional)
  #   configMap: log4j-config
  #   readOnly: true

  ## @param sparkoperator.env Extra environment variables that will be pass onto deployment pods
  ##
  env: {}

  ## @param sparkoperator.sparkTemplates.driver Template yaml to define the driver pod
  ## @param sparkoperator.sparkTemplates.executor Template yaml to define the executor pod
  ##
  sparkTemplates:
    driver:
    executor:

  ## @param sparkoperator.nodeSelector Node labels for update Operator pod assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  ##
  nodeSelector: {}

  ## @param sparkoperator.tolerations Tolerations for update Operator pod assignment
  ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  ##
  tolerations: []

  ## Spark Operator container's resource requests and limits
  ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
  ## @param sparkoperator.resources.limits The resources limits for the Controller container
  ## @param sparkoperator.resources.requests The requested resources for the Controller container
  ##
  resources:
    limits: {}
    requests: {}


## Online-Store service deployment parameters
##
onlinestore:
  ## @param onlinestore.replicaCount Number of Online-Store Service replicas
  ##
  replicaCount: 2

  ## @param onlinestore.commonLabels Labels to add to all deployed objects
  ##
  commonLabels: {}

  ## Online-Store Service Autoscaling configuration
  ## ref: https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
  ## @param autoscaling.enabled Enable Horizontal POD autoscaling for Online-Store Service
  ## @param autoscaling.minReplicas Minimum number of Online-Store Service replicas
  ## @param autoscaling.maxReplicas Maximum number of Online-Store Service replicas
  ## @param autoscaling.targetCPU Target CPU utilization percentage
  ## @param autoscaling.targetMemory Target Memory utilization percentage
  ##
  autoscaling:
    enabled: false
    minReplicas: 2
    maxReplicas: 10
    targetCPU: 50
    targetMemory: 50

  ## @param onlinestore.config.onlineOfflineIngestionCutoffTimeInMinutes Cutoff time to trigger ingestion from online to offline. Expressed in minutes
  config:
    onlineOfflineIngestionCutoffTimeInMinutes: 15

  ## @param onlinestore.docker.image Online Store Service Docker image path
  ## @param onlinestore.docker.tag Online Store Service image tag
  ##
  docker:
    image: "h2oai/feature-store-online-store"
    tag:

  ## @param onlinestore.extraConfigmapMounts configmaps to mount in core pods
  ##
  extraConfigmapMounts: []
  # - name: log4j-config
  #   mountPath: /opt/h2oai/config/
  #   subPath: log4j2.properties # (optional)
  #   configMap: log4j-config
  #   readOnly: true

  ## @param onlinestore.env Extra environment variables that will be pass onto deployment pods
  ##
  env: {}

  ## @param onlinestore.nodeSelector Node labels for update Core pods assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  ##
  nodeSelector: {}

  ## @param onlinestore.tolerations Tolerations for update Core pods assignment
  ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  ##
  tolerations: []

  ## Online Store containers' resource requests and limits
  ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
  ## @param onlinestore.resources.limits The resources limits for the Controller container
  ## @param onlinestore.resources.requests The requested resources for the Controller container
  ##
  resources:
    limits: {}
    requests: {}


sparkhistory:
  ## @param sparkhistory.commonLabels Labels to add to all deployed objects
  ##
  commonLabels: {}
  ## @param sparkhistory.docker.image Spark History Service Docker image path
  ## @param sparkhistory.docker.tag Spark History Service image tag
  ##
  docker:
    image: "h2oai/feature-store-spark"
    tag:

  ## @param sparkhistory.uiSecrets.username Username for access to Spark History UI
  ## @param sparkhistory.docker.tag Password for access to Spark History UI
  ##
  uiSecrets:
    username:
    password:

  ## @param sparkhistory.nodeSelector Node labels for update Operator pod assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  ##
  nodeSelector: { }

  ## @param sparkhistory.tolerations Tolerations for update Operator pod assignment
  ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  ##
  tolerations: [ ]

  ## Spark History container's resource requests and limits
  ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
  ## @param sparkhistory.resources.limits The resources limits for the Spark history container
  ## @param sparkhistory.resources.requests The requested resources for the Spark history container
  ##
  resources:
    limits: {}
    requests: {}

## Configure the ingress resource that allows you to access the Feature-Store installation
## ref: https://kubernetes.io/docs/user-guide/ingress/
##
ingress:
  ## @param ingress.apiVersion Ingress API version
  ##
  apiVersion: networking.k8s.io/v1

  api:
    ## @param ingress.annotations Additional annotations for the Ingress resource. To enable certificate autogeneration, place here your cert-manager annotations.
    ## For a full list of possible ingress annotations, please see
    ## ref: https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/annotations.md
    ## Use this parameter to set the required annotations for cert-manager, see
    ## ref: https://cert-manager.io/docs/usage/ingress/#supported-annotations
    ##
    ## e.g:
    ## annotations:
    ##   kubernetes.io/ingress.class: nginx
    ##   nginx.ingress.kubernetes.io/backend-protocol: GRPC
    ##   nginx.ingress.kubernetes.io/grpc-backend-for-port: fs-api-port
    ##   nginx.ingress.kubernetes.io/server-snippet: grpc_read_timeout 3600s; grpc_send_timeout
    ##     3600s;
    ##
    annotations:

    ## @param ingress.hostname Default host for the ingress record
    ##
    hostname:

    ## @param ingress.path Path for the API endpoint
    ##
    path: /

    ## @param ingress.tls Enable TLS configuration for the host defined at `ingress.hostname` parameter
    ## TLS certificates will be retrieved from a TLS secret with name: `{{- printf "%s-tls" .Values.ingress.hostname }}`
    ## You can:
    ##   - Use the `ingress.secrets` parameter to create this TLS secret
    ##   - Relay on cert-manager to create it by setting the corresponding annotations
    ##   - Relay on Helm to create self-signed certificates by setting `ingress.selfSigned=true`
    ##
    tls: false

  web:
    ## @param ingress.annotations Additional annotations for the Ingress resource. To enable certificate autogeneration, place here your cert-manager annotations.
    ## For a full list of possible ingress annotations, please see
    ## ref: https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/annotations.md
    ## Use this parameter to set the required annotations for cert-manager, see
    ## ref: https://cert-manager.io/docs/usage/ingress/#supported-annotations
    ##
    ## e.g:
    ## annotations:
    ##   kubernetes.io/ingress.class: nginx
    ##   cert-manager.io/cluster-issuer: cluster-issuer-name
    ##
    annotations:

    ## @param ingress.hostname Default host for the ingress record
    ##
    hostname:

    ## @param ingress.callbackPath Path for the IdP redirect url
    ##
    callbackPath: /Callback

    ## @param ingress.tls Enable TLS configuration for the host defined at `ingress.hostname` parameter
    ## TLS certificates will be retrieved from a TLS secret with name: `{{- printf "%s-tls" .Values.ingress.hostname }}`
    ## You can:
    ##   - Use the `ingress.secrets` parameter to create this TLS secret
    ##   - Relay on cert-manager to create it by setting the corresponding annotations
    ##   - Relay on Helm to create self-signed certificates by setting `ingress.selfSigned=true`
    ##
    tls: false

## Telemetry service deployment parameters
##
telemetry:
  ## @param telemetry.replicaCount Number of Telemetry Service replicas
  ##
  replicaCount: 1

  ## @param telemetry.commonLabels Labels to add to all deployed objects
  ##
  commonLabels: {}

  ## @param telemetry.docker.image Telemetry Service Docker image path
  ## @param telemetry.docker.tag Telemetry Store Service image tag
  ##
  docker:
    image: "h2oai/feature-store-online-store"
    tag:

  ## @param telemetry.window.timeduration how often the aggregation should occur (in minutes)
  ##
  window:
    timeDuration:

  ## @param telemetry.service.host telemetry service address
  ##
  service:
    host:

  ## @param telemetry.nodeSelector Node labels for update Telemetry pods assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  ##
  nodeSelector: {}

  ## @param telemetry.tolerations Tolerations for update Telemetry pods assignment
  ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  ##
  tolerations: []

  ## Telemetry containers' resource requests and limits
  ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
  ## @param telemetry.resources.limits The resources limits for the Controller container
  ## @param telemetry.resources.requests The requested resources for the Controller container
  ##
  resources:
    limits: {}
    requests: {}

  ## @param telemetry.extraConfigmapMounts configmaps to mount in core pods
  ##
  extraConfigmapMounts: []
  # - name: log4j-config
  #   mountPath: /opt/h2oai/config/
  #   subPath: log4j2.properties # (optional)
  #   configMap: log4j-config
  #   readOnly: true

  ## @param telemetry.env Extra environment variables that will be pass onto deployment pods
  ##
  env: {}
